## Vue App Demo

### Purpose

This a demo app, part of a wider project meant to learn kubernetes, docker, gitlab, vue, etc

Future dev ideas

1. Basic vue components
2. Learn routing
3. Use Pinia storage
4. Login with fb / google

### Development

#### Prerequisites

1. `docker` installed, without sudo
2. `docker-compose`

#### Docker local

Local development is done via docker. Either start in production mode, or in dev mode via available scripts

1. Production mode

```
docker-compose -f ./infra/docker/compose/docker-compose.local-prod.yml up
```

2. Dev mode - hot reload

```
ATM without docker via "npm run dev"
```

